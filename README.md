Migrate Process Text to Paragraphs
================
This module provides a Migrate process plugin to allow processing of string
with new lines into string with paragraphs (similiar to nl2br()).

## Example

```
process:
  'body/value':
    -
      plugin: get
      source: text
    -
      plugin: migrate_process_text_to_paragraphs
      delimiter: \n // default \n\n
```

Author
-----------
* Daniel Lobo (2dareis2do)
