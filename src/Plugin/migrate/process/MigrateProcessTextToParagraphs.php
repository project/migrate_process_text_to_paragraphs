<?php

namespace Drupal\migrate_process_text_to_paragraphs\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'MigrateProcessTextToParagraphs' migrate process plugin.
 *
 * Example:
 *
 * @code
 * process:
 *   'body/value':
 *     -
 *       plugin: get
 *       source: text
 *     -
 *       plugin: migrate_process_text_to_paragraphs
 *       delimiter: \n // default \n\n
 *
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *  id = "migrate_process_text_to_paragraphs"
 * )
 */
class MigrateProcessTextToParagraphs extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Logger Class object.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a database object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $delimiter = "\n\n";
    $delimiter = $this->delimiterEscapeSequence($delimiter);
    $configuration = $this->configuration;

    // Check if we need to disable default support for JavaScript redirects.
    if (isset($configuration["delimiter"])) {
      $delimiter = $this->delimiterEscapeSequence($configuration["delimiter"]);
    }
    // This is useful as we can also set from the unit tests.
    if (isset($this->pluginDefinition) && isset($this->pluginDefinition["delimiter"])) {
      $delimiter = $this->delimiterEscapeSequence($this->pluginDefinition["delimiter"]);
    }

    $new_value = $this->nl2p($delimiter, $value);

    return $new_value;
  }

  /**
   * Wraps strings ending in new line delimiters in a html paragraph tag.
   *
   * @param string $delimiter
   *   Delimiter used for line endings.
   * @param string $value
   *   String to update.
   *
   * @return string
   *   Converted from single to double enclosed quotes.
   */
  public function nl2p(string $delimiter, string $value) {
    $lines = explode($delimiter, $value);
    $paragraphs = [];
    foreach ($lines as $key => $line) {
      // Trim whitespace and check if the line is not empty.
      $eol = '';
      if ($key !== array_key_last($lines)) {
        $eol = PHP_EOL . PHP_EOL;
      }
      $paragraphs[] = '<p>' . $line . '</p>' . $eol;
    }
    $imploded = implode('', $paragraphs);

    return $imploded;
  }

  /**
   * Uses delimiters to convert string.
   *
   *   See https://www.php.net/manual/en/language.types.string.php.
   *
   * @param string $delimiter
   *   See https://www.php.net/manual/en/reserved.constants.php#constant.php-eol
   *   .
   *
   * @return string
   *   Converted from single to double enclosed quotes.
   */
  public function delimiterEscapeSequence(string $delimiter) {
    $order = ['\r\n', '\n\r', '\n', '\r'];
    $replace = ["\r\n", "\n\r", "\n", "\r"];

    // Processes \r\n's first so they aren't converted twice.
    $double_quoted = str_replace($order, $replace, $delimiter);

    return $double_quoted;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.migrate_process_text_to_paragraphs')
    );
  }

}
