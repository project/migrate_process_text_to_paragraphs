<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_process_text_to_paragraphs\Unit\process;

use Drupal\migrate_process_text_to_paragraphs\Plugin\migrate\process\MigrateProcessTextToParagraphs;
use Drupal\Tests\migrate\Unit\process\MigrateProcessTestCase;
use Psr\Log\LoggerInterface;

/**
 * Tests the migrate_process_text_to_paragraphs process plugin.
 *
 * @group migrate_process_text_to_paragraphs
 * @coversDefaultClass \Drupal\migrate_process_text_to_paragraphs\Plugin\migrate\process\MigrateProcessTextToParagraphsTest
 */
final class MigrateProcessTextToParagraphsTest extends MigrateProcessTestCase {

  /**
   * A logger prophecy object.
   *
   * Using ::setTestLogger(), this prophecy will be configured and injected into
   * the container. Using $this->logger->function(args)->shouldHaveBeenCalled()
   * you can assert that the logger was called.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $logger;

  /**
   * Mock of http_client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $mockHttp;

  /**
   * The data fetcher plugin definition.
   */
  private array $pluginDefinition = [
    'id' => 'migrate_process_text_to_paragraphs',
    'title' => 'Migrate Process Text to Paragraphs',
    'delimiter' => '\n\n',
  ];

  /**
   * Test MigrateProcessTextToParagraphs with default delimiter.
   */
  public function testMigrateProcessTextToParagraphsDefault() {
    $configuration = [];

    $json = file_get_contents(__DIR__ . '/../../../fixtures/files/web/modules/custom/migrate_process_text_to_paragraphs/tests/fixtures/files/_www.lbc.co.uk_1709232471984.json');
    $associative_array = json_decode($json, TRUE);

    $delimiter = "\n\n";
    $value = $associative_array['text'];

    $lines_correct = explode($delimiter, $value);
    $initial_count = count($lines_correct);

    $this->logger = $this->prophesize(LoggerInterface::class);

    $document = (new MigrateProcessTextToParagraphs($configuration, 'migrate_process_text_to_pargraphs', $this->pluginDefinition, $this->logger->reveal()))
      ->transform($value, $this->migrateExecutable, $this->row, 'destinationproperty');

    $lines_test = explode($delimiter, $document);
    $new_count = count($lines_test);

    // This should return the same amount of new lines.
    // @todo May be better to count number of paragraphs === number of nw lines?
    $this->assertEquals($initial_count, $new_count, $message = "actual value is not equal to expected");
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    $this->row = $this->getMockBuilder('Drupal\migrate\Row')
      ->disableOriginalConstructor()
      ->getMock();

    $this->migrateExecutable = $this->getMockBuilder('Drupal\migrate\MigrateExecutable')
      ->disableOriginalConstructor()
      ->getMock();

    parent::setUp();

  }

}
